<br>Cara Install</br>
1. Install docker dan docker-compose
2. Buat folder data/elasticsearch1, data/elasticsearch2 dan data/elasticsearch3
3. Jalankan docker dengan command "docker-compose up -d"

note:
 - Data elasticsearch tersimpan pada mesin host. Jadi walaupun container di delet, selama folder data tidak didelet. Data masih ada.
 - Untuk stop semua container dapan menjalankan command "docker-compose stop" dan delet semua data didalam folder data/elasticsearch1, data/elasticsearch2 dan data/elasticsearch3

<br>Akses Elasticsearch</br>
Elasticsearch
 - node1 = http://localhost:19200
 - node2 = http://localhost:29200
 - node3 = http://localhost:39200
 - node-monitoring = http://localhost:9200

Kibana
 - kibana cluster dashboard = http://localhost:15601
 - kibana cluster monitoring = http://localhost:25601

<br>Error yang biasa timbul</br>
1. Container elasticsearch stop dan ada error <br>max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]</br> pada docker logs.
    - Jika ubuntu, jalankan <br>sudo sysctl -w vm.max_map_count=262144</br> sebelum jalankan docker-compose up -d
